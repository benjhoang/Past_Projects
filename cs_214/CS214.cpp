/*******************************************************************
*   CS 214 Programming Assignment
*   program 1: The Set Relator Computer
*   Author: Benjamin Hoang
*	Date: 11/24/2015 
*   I attest that this program is entirely my own work
*****************************************************************/

#include<iostream>
#include<string>
#include<fstream>
#include <algorithm>
#include<vector>
using namespace std;

//Functions 1-7
void Display_Instrunctions();//display all instructions
void Get_Data_Input(int (&Data_storage)[25][2] , vector<int> &Vector_Set_A, vector<int> &Vector_Set_B);//Get Information from data file(rel.dat)
void Cal_Set_A_Cardinality(int &Cardinality_Set_A);	//function 1: Calc. |A|
void Cal_Set_B_Cardinality(int &Cardinality_Set_B);	//funtion 2: Calc. |B|
void Cal_Relation_is_Function(bool &Not_relation_function);	//function 3: Calc. Relation of set A and B
void Cal_Set_Intersection (vector<int> &Intersection_AB);	//function 4: Calc. Intersection of Set A and B
void Cal_Intersection_Cardinality(int &Cardinality_AB); //function 5: Calc. Cardinality of Intersection Of Set A and B
void Cal_Union_Princ_Incl_Excl(int &Cardinality_U_AB);	//function 6: Calc. Cardinality of |AUB|
void exit_function(bool &Quit_function);	//function 7: Quit Programm

int main()
{
	//variables declaration and initialization
	vector<int> Intersection_AB;
	int Cardinality_A, Cardinality_B, Cardinality_Intersection_AB, Cardinality_U_AB;
	ofstream Data_outfile;
	bool Quit_function = false;
	bool Not_relation_function = true;
	int User_Input;

	//Display instructions
	Display_Instrunctions();
	//output data to file OutRel.dat
	Data_outfile.open("C:/Temp/OutRel.dat");	//Create output.txt file
	if (!Data_outfile.is_open())exit(0);	//check if file opens correctly

	
	cin >> User_Input;
	while (Quit_function == false)	//Check when to quit program.
	{
		if (User_Input == 1)
		{
			Cal_Set_A_Cardinality(Cardinality_A);	//Call function 1
			Data_outfile << "Cardinality of A: " << Cardinality_A << endl;
			cout << "Input another option: ";
			cin >> User_Input;

		}
		
		else if (User_Input == 2)	
		{
			Cal_Set_B_Cardinality(Cardinality_B);	//Call funtion 2
			Data_outfile << "Cardinality of B: " << Cardinality_B << endl;
			cout << "Input another option: ";
			cin >> User_Input;
		}
		else if (User_Input == 3)	
		{
			Cal_Relation_is_Function(Not_relation_function);	 //Call funtion 3
			if (Not_relation_function == true)
			{
				Data_outfile << "Relation is not a function" << endl;
			}
			if (Not_relation_function == false)
			{
				Data_outfile << "Relation is a funtion" << endl;
			}
			cout << "Input another option: ";
			cin >> User_Input;
		}
		else if (User_Input == 4)	
		{
			Cal_Set_Intersection(Intersection_AB);	//Call function 4
			Data_outfile << "Intersection of Set A and B: (";
			for (unsigned int i = 0; i < Intersection_AB.size(); i++)
			{

				Data_outfile << Intersection_AB[i] << ", ";
			}

			Data_outfile <<")"<< endl;
			cout << "Input another option: ";
			cin >> User_Input;
		}
		else if (User_Input == 5)
		{
			Cal_Intersection_Cardinality(Cardinality_Intersection_AB);	//Call function 5
			Data_outfile << "Cardinality Intersection of set A and B: "<< Cardinality_Intersection_AB<<endl;
			cout << "Input another option: ";
			cin >> User_Input;
		}
		else if (User_Input == 6)
		{
			Cal_Union_Princ_Incl_Excl(Cardinality_U_AB);	//Call function 6
			Data_outfile << "Cardinality of Union A and B: " << Cardinality_U_AB << endl;
			cout << "Input another option: ";
			cin >> User_Input;
		}
		else if(User_Input == 7)
		{
			exit_function(Quit_function);	//Call function 7
		}
		else
		{
			cout << "Please, enter from 1 to 7: " << endl;
			cin >> User_Input;
		}
	}
	Data_outfile.close();

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//function to get data from rel.dat to 2-D array
void Get_Data_Input(int(&Data_storage)[25][2], vector<int> &Vector_Set_A, vector<int> &Vector_Set_B)
{
	int Tuples_size = 0;
	int Data_Storage[25][2];
	int count = 0;
	ifstream Data_inFile;//data input from .dat file
	Data_inFile.open("C:/Temp/rel.dat");
	if (!Data_inFile.is_open())exit(0); //check if file opens correctly
										//loop to get all data from rel.dat file
	while (!Data_inFile.eof())
	{
		for (int i = 0; i < 25; i++)
		{
			for (int k = 0; k < 2; k++)
			{
				Data_inFile >> Data_Storage[i][k];
				//if loop to count for how many tuples
				if (Data_Storage[i][k] != 0)
				{
					count = count++;
				}
			}
		}
		Tuples_size = count / 2; //tuples size
	}
	
	//store set A to Vector

	for (int i = 0; i < Tuples_size; i++)
	{
		if (Data_Storage[i][0] >0 && Data_Storage[i][0] <26)
		{
			Vector_Set_A.push_back(Data_Storage[i][0]);
		}
	}
	//store set B to Vector
	

	for (int i = 0; i < Tuples_size; i++)
	{
		if (Data_Storage[i][0] >0 && Data_Storage[i][0] <26)
		{
			Vector_Set_B.push_back(Data_Storage[i][1]);
		}
	}
}
//function 1
void Cal_Set_A_Cardinality(int &Cardinality_Set_A)
{
	//create variables & pass by ref Get_Data_Input
	vector<int> Set_A;
	vector<int> Set_B;
	int Data_storage[25][2];
	Get_Data_Input(Data_storage, Set_A, Set_B); 

	int count = Set_A.size();

	sort(begin(Set_A), end(Set_A)); //sort Set_A from smallest to biggest
	
	
	Set_A.erase(unique(Set_A.begin(), Set_A.end()), Set_A.end());	//remove all duplicate elements
	
	/*
	//option 2:
	int count = Set_A.size()-1;
	Cardinality_Set_A = Set_A.size();

	for (int i = 0; i < count; i++)
	{
		if (Set_A[i] == Set_A[i + 1])
		{
			Cardinality_Set_A = Cardinality_Set_A--;
		}
	}

	*/
	Cardinality_Set_A = Set_A.size();	//Get Cardinality of Set A
	
}
//function 2
void Cal_Set_B_Cardinality(int &Cardinality_Set_B)
{
	//create variables & pass by ref Get_Data_Input
	vector<int> Set_A;
	vector<int> Set_B;
	int Data_storage[25][2];
	Get_Data_Input(Data_storage, Set_A, Set_B);

	sort(begin(Set_B), end(Set_B));
	//calculate for cardinality
	Set_B.erase(unique(Set_B.begin(), Set_B.end()), Set_B.end());
	
	Cardinality_Set_B = Set_B.size();	//Get Cardinality of Set B
}

//function 3
void Cal_Relation_is_Function(bool &Not_relation_function)
{
	//create variables & pass by ref Get_Data_Input
	vector<int> Set_A;
	vector<int> Set_B;
	int Data_Storage[25][2];
	Get_Data_Input(Data_Storage, Set_A, Set_B);

	for (int i = 0; i < 25; i++) //for loop check for "one to many"
	{
		if (Data_Storage[i][0] != 0 && Data_Storage[i][1] != 0)//skip all empty tuples (0,0)
		{
			int x, y;
			x = Data_Storage[i][0];
			y = Data_Storage[i][1];

			for (int k = 0; k < 25; k++)
			{
				if (x == Data_Storage[k][0])
				{
					if (y != Data_Storage[k][1])
					{
						Not_relation_function = false;
					}
				}
			}
		}

	}
}

//function 4
void Cal_Set_Intersection(vector<int> &Intersection_AB)
{
	//create variables & pass by ref Get_Data_Input
	vector<int> Set_A;
	vector<int> Set_B;
	int Data_Storage[25][2];
	Get_Data_Input(Data_Storage, Set_A, Set_B);

	for (unsigned int i = 0; i < Set_A.size(); i++)		//compare Set A to Set B, if same elements then add vector Intersection_AB
	{
		for (unsigned int j = 0; j < Set_B.size(); j++)
		{
			if (Set_A[i] == Set_B[j])
			{
					Intersection_AB.push_back(Set_A[i]);
			}

		}
	}
	sort(begin(Intersection_AB), end(Intersection_AB));
	Intersection_AB.erase(unique(Intersection_AB.begin(), Intersection_AB.end()), Intersection_AB.end()); //remove duplicate element in vector AB.
}
//function 5
void Cal_Intersection_Cardinality(int &Cardinality_AB)
{
	vector<int> Intersection_AB;
	Cal_Set_Intersection(Intersection_AB);	//Call function 4
	Cardinality_AB = Intersection_AB.size();	//Get cardinality AB
}

void Cal_Union_Princ_Incl_Excl(int &Cardinality_U_AB)
{

	//|AUB|=|A|+|B|-|intersection AB|
	int Cardinality_A, Cardinality_B, Cardinality_Intersection_AB;
	Cal_Set_A_Cardinality(Cardinality_A);	//Call function 1
	Cal_Set_B_Cardinality(Cardinality_B);	//Call function 2
	Cal_Intersection_Cardinality(Cardinality_Intersection_AB);	//Call function 5
	Cardinality_U_AB = Cardinality_A + Cardinality_B - Cardinality_Intersection_AB;	//Get cardinality of Union A and B
	

}
void Display_Instrunctions()
{
	cout << "Option 1: Find and Output Set A Cardinality." << endl;
	cout << "Option 2: Find and Output Set B Cardinality." << endl;
	cout << "Option 3: Find and Output Relation of Set A and B." << endl;
	cout << "Option 4: Find and Output Items in Set Intersection." << endl;
	cout << "Option 5: Find and Output Intersection Cardinality." << endl;
	cout << "Option 6: Find and Output Cardinality of Set Union using Princ. Incl./Excl. " << endl;
	cout << "Option 7: ~~~ Quit Program ~~~" << endl;
	cout << "Please, enter your option(1-7): ";

}

void exit_function(bool &Quit_function)
{

	Quit_function = true; //quit while loop



}