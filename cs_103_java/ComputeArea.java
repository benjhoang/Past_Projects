/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bth0006
 */
import java.util.Scanner;
public class ComputeArea {
    public static void main(String[] args)
    {
        double radius = 10.0; //radius of circle
        double Pi = 3.14159;//pie of circle
        
        //calculate area of circle
        double area = Pi * radius * radius;
        //output the result
        System.out.println("Area of a circle");
        System.out.println("Radius: "+radius);
        System.out.println("Area: "+area);
        
        
        radius = 6.25; //radius of circle
        area = Pi * radius * radius;//calculate area of circle
          //output the result
        System.out.println("Area of a circle");
        System.out.println("Radius: "+radius);
        System.out.println("Area: "+area);
        
        
        
        //get input from user
        Scanner user_input = new Scanner(System.in);
        System.out.println("Input radius:");
        double user_radius = user_input.nextDouble();
        
        area = Pi * user_radius+ user_radius; //calculate area
        //output the result
        System.out.println("Area of a circle");
        System.out.println("Radius: "+user_radius);
        System.out.println("Area: "+area);
        
    }
    
}
