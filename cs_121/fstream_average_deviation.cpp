/*******************************************************************
*   CS 121 Programming Assignment 2
*   File: Source Code File Name
*   Author: Benjamin Hoang
*   Desc: A brief description of what the program does.
*   Date: Date file was submitted to the instructor
*
*   I attest that this program is entirely my own work
*******************************************************************/

#include<iostream>
#include<fstream>
using namespace std;
void Equation_01();//first part of the equation
void Equation_02();//second part of the equation
double Total_A, Total_B, N, S, Average;
int main()
{
	Equation_01(); // call for function equation_01

	Equation_02(); //call for function equation_02

		//calculate for standard deviation
		S = sqrt((Total_A / N) - (Total_B / N)*(Total_B / N));
		//calculate average
		Average = Total_B / N;
		cout << "Standard Deviation : "<<S << endl;
		cout << "average : " << Average;

		return 0;
}
void Equation_01() //calculate first part of the equation
{
	int counter = 0;
	double Score = 0.0;
	double sum = 0.0;
	
	ifstream ScoresFile;

	ScoresFile.open("scores.txt");
	if (!ScoresFile.is_open())exit(0);

	while (!ScoresFile.eof())
	{
		ScoresFile >> Score;
		sum = sum + Score*Score;//Sum of(x^2)
		counter = counter++;//to get n

	}
	N = counter;
	Total_A = sum;
	cout << "Sum of X^2 : " << sum << endl;
	cout << "N (the number of values) : " << counter << endl;
}
void Equation_02() //calculate second part of the equation
{
	double Score = 0.0;
	double sum = 0.0;

	ifstream ScoresFile;

	ScoresFile.open("scores.txt");
	if (!ScoresFile.is_open())exit(0);

	while (!ScoresFile.eof())
	{
		ScoresFile >> Score;
		sum = sum + Score;//Sum of(x)
	}
	Total_B = sum;
	cout << "Sum of X: " << Total_B << endl;
}