/*******************************************************************
*   CS 121 Programming Assignment 1
*   File: Source.cpp
*   Author: Benjamin Hoang
*   Desc: Programming Assignment 1
*   Date: 10/12/2015
*
*   I attest that this program is entirely my own work
*******************************************************************/
#include<iostream> //this is function for cout and cin
using namespace std;	//standard header file

int main()
{
	//declaration and initialization 
	double Radius = 5.0;
	double Height = 20.0;
	double Pi = 3.14159265359;
	double ConeVolume;
	
	//Calculation
	ConeVolume = Pi*Radius*Radius*(Height / 3);
	
	//output to screen
	cout << "Cone's Height = " << Height << "	Cone's Radius = " << Radius << "	Cone's volume = " << ConeVolume << endl;
	return 0;

}


