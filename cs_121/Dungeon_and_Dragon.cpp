/*******************************************************************
*   CS 121 Programming Assignment 4 _ phase 8
*   File: Source.cpp
*   Author: Benjamin Hoang
*   Desc: this is a game project. In this game, players need to solve the maze, collect all 4 treasures to escape.
*   Date: 11/24/2015
*
*   I attest that this program is entirely my own work
*******************************************************************/
#include<iostream> //this is function for cout and cin
#include<string>

//define doors 
#define Door_North	0x00000001
#define Door_East	0x00000002
#define Door_South	0x00000004
#define Door_West	0x00000008

//define treasure items
#define Sword_of_Courage	0x00000010
#define	Hat_of_Knowledge	0x00000020
#define	Ring_of_Power		0x00000040
#define	Mystical_Beast		0x00000080

//define dangerous creatues
#define	King_Cobra		0x00000100
#define Dragon			0x00000200
#define	Venomous_Spider	0x00000400



using namespace std;
int TakeItem(unsigned int rooms[4][4], int row, int col, string Object);
int FightFunction(unsigned int rooms[4][4], int row, int col, string Object);
void BuildRooms(unsigned int rooms[4][4]);
void DescribeRoomContents(int roomDesc, int row, int col);
void DescribeRoom_01();
void DescribeRoom_02();
void DescribeRoom_03();
void DescribeRoom_04();
void DescribeRoom_05();
void DescribeRoom_06();
void DescribeRoom_07();
void DescribeRoom_08();
void DescribeRoom_09();
void DescribeRoom_10();
void DescribeRoom_11();
void DescribeRoom_12();
void DescribeRoom_13();
void DescribeRoom_14();
void DescribeRoom_15();
void DescribeRoom_16();

int main()
{
	//Player location and size Grid
	int CurrentRow = 0;
	int CurrentCol = 0;
	int GridSize_Rows = 4;
	int GridSize_Cols = 4;

	//player possessions variable

	int iplayer = 0;
	int endRow = 3;
	int endCol = 3;


	//string object to hold player's command
	string cmdVerb;
	string cmdObj;
	//create init 2-D array
	unsigned int TheMaze[4][4];
	//initially array to zero
	for (int row = 0; row < 4; row++)
	{
		for (int col = 0; col < 4; col++)
		{
			TheMaze[row][col] = 0;
		}
	}

	//build the rooms
	BuildRooms(TheMaze);
	//print the game instruction to cmd
	cout << "	~~~~~~Welcome to Maze_The_Matrix~~~~~~" << endl;
	cout << "	Solve the maze and claim your victory" << endl << endl;
	cout << "Along the journey into the maze, you will find four valueable items; " << endl;
	cout << "(Sword of Courage, Hat of Knowledge, Ring of Power, and one mystical Beast" << endl;
	cout << "Collect all 4 treasures and go to exit door to ....WIN) " << endl;
	cout << "You will start at one end of the maze. Your goal is to find all treasures/beast and find a way to the exit door." << endl;
	cout << "You will face dangerous creatures along the way" << endl;
	cout << "Survive and find a way out of the maze." << endl;
	cout << "				~~~~Good luck!~~~~~" << endl << endl;
	cout << "			press the Enter key to begin\n";
	getc(stdin);
	DescribeRoom_01();



	//game loop
	bool bQuitGame = false;
	while (bQuitGame == false)//quit function
	{
		//get cmd from player
		cout << "What do you want to do? (GO, TAKE, FIGHT ===>> NORTH, SOUTH, EAST, WEST.....  )" << endl;
		cin >> cmdVerb; //GO, TAKE, FIGHT
		cin >> cmdObj;  //NORTH, SOUTH, EAST, WEST


		//change player's commands to all caps
		int cmdVerb_length = cmdVerb.size();
		for (int idx = 0; idx < cmdVerb_length; idx++)
		{
			cmdVerb[idx] = toupper(cmdVerb[idx]);
		}

		int cmdObj_length = cmdObj.size();
		for (int idx = 0; idx < cmdObj_length; idx++)
		{
			cmdObj[idx] = toupper(cmdObj[idx]);
		}



		if (cmdVerb == "GO") //check cmdverb GO
		{
			
			if (cmdObj == "NORTH")
			{

				if (TheMaze[CurrentRow][CurrentCol] & Door_North)
				{

					CurrentRow--;
					DescribeRoomContents(TheMaze[CurrentRow][CurrentCol], CurrentRow, CurrentCol);
					cout << "command verb is: GO" << endl;
					cout << "Direction is NORTH" << endl;
				}
				else
				{
					cout << "There is no door to the NORTH" << endl;
				}
			}
			else if (cmdObj == "SOUTH")
			{
				if (TheMaze[CurrentRow][CurrentCol] & Door_South)
				{
					CurrentRow++;
					DescribeRoomContents(TheMaze[CurrentRow][CurrentCol], CurrentRow, CurrentCol);
					cout << "command verb is: GO" << endl;
					cout << "Direction is SOUTH" << endl;
				}
				else
				{
					cout << "There is no door to the SOUTH" << endl;
				}
			}
			else if (cmdObj == "EAST")
			{
				if (TheMaze[CurrentRow][CurrentCol] & Door_East)
				{
					CurrentCol++;
					DescribeRoomContents(TheMaze[CurrentRow][CurrentCol], CurrentRow, CurrentCol);
					cout << "command verb is: GO" << endl;
					cout << "Direction is EAST" << endl;

				}
				else
				{
					cout << "There is no door to the EAST" << endl;
				}

			}
			else if (cmdObj == "WEST")
			{
				if (TheMaze[CurrentRow][CurrentCol] & Door_West)
				{
					CurrentCol--;
					DescribeRoomContents(TheMaze[CurrentRow][CurrentCol], CurrentRow, CurrentCol);
					cout << "command verb is: GO" << endl;
					cout << "Direction is WEST" << endl;
				}
				else
				{
					cout << "There is no door to the WEST" << endl;
				}


			}
			else
			{
				cout << "I don't understand: "
					<< cmdObj << endl;
			}

			if ((CurrentRow == endRow) && (CurrentCol == endCol))
			{
				if ((iplayer & Sword_of_Courage) &&
					(iplayer & Hat_of_Knowledge) &&
					(iplayer & Ring_of_Power) &&
					(iplayer & Mystical_Beast))
				{

					cout << "$$$&&&___congratulation___&&&$$$"<< endl;
					cout<<	"You collect all 4 treasures" << endl;
					cout << "And enter exit door alive." << endl;
					cout << "_______You win!!!_________" << endl;
					bQuitGame = true;
				}

			}

		}

		else if (cmdVerb == "TAKE")//check cmdverb Take
		{
			int takeVal = TakeItem(TheMaze, CurrentRow, CurrentCol, cmdObj);
			if (takeVal == 16)
			{
				iplayer |= takeVal;
				cout << "You are now carrying the Sword" << endl;

			}
			else if (takeVal == 32)
			{
				iplayer |= takeVal;
				cout << "You are now carrying the Hat" << endl;

			}
			else if (takeVal == 64)
			{
				iplayer |= takeVal;
				cout << "You are now carrying the Ring" << endl;

			}
			else if (takeVal == 128)
			{
				iplayer |= takeVal;
				cout << "You are now riding on a Beast" << endl;

			}
			else
			{
				cout << "You can't take the " << cmdObj << "from this location. " << endl;
			}
		}
		else if (cmdVerb == "FIGHT")//check cmdverb fight
		{

			int FightVal = FightFunction(TheMaze, CurrentRow, CurrentCol, cmdObj);
			//dangerous creatures
			if (FightVal == 256)
			{
				cout << "Sorry, you got bitten by a King Cobra!!!" << endl;
				cout << "END GAME!!!" << endl;
				break;
				bQuitGame = true;

			}
			//dangerous creatures
			else if (FightVal == 512)
			{
				cout << "Sorry, you die from dragon's fire!!!" << endl;
				cout << "END GAME!!!" << endl;
				bQuitGame = true;
			}
			else if (FightVal == 1024)
			{
				cout << "Sorry, you got bitten by a Venomous Spider!!!" << endl;
				cout << "END GAME!!!" << endl;
				bQuitGame = true;
			}
			else
			{
				cout << "There is none for you to fight!!! " << endl;
			}

		}

		else if (cmdVerb == "QUIT")//QUIT GAME 
		{
			cout << "Too hard for you? "<<endl;
			cout << "   Quit Game!" << endl;
			bQuitGame = true;
		}
		else
		{
			cout << "I don't understand " << cmdVerb << endl;
		}

	}

}



void DescribeRoomContents(int roomDesc, int row, int col)
{

	switch (row)
	{
	case 0:
		switch (col)
		{
		case 0:
			DescribeRoom_01();
			break;
		case 1:
			DescribeRoom_02();
			break;
		case 2:
			DescribeRoom_03();
			break;
		case 3:
			DescribeRoom_04();
			break;
		}
		break; //break from case row 0
	case 1:
		switch (col)
		{
		case 0:
			DescribeRoom_05();
			break;
		case 1:
			DescribeRoom_06();
			break;
		case 2:
			DescribeRoom_07();
			break;
		case 3:
			DescribeRoom_08();
			break;
		}
		break; //break from case row 1
	case 2:
		switch (col)
		{
		case 0:
			DescribeRoom_09();
			break;
		case 1:
			DescribeRoom_10();
			break;
		case 2:
			DescribeRoom_11();
			break;
		case 3:
			DescribeRoom_12();
			break;
		}
		break;//break from case row 2

	case 3:
		switch (col)
		{
		case 0:
			DescribeRoom_13();
			break;
		case 1:
			DescribeRoom_14();
			break;
		case 2:
			DescribeRoom_15();
			break;
		case 3:
			DescribeRoom_16();
			break;
		}
		break; //breack from case row 3
	}

	//describe the creatures
	if (roomDesc & King_Cobra)
	{
		cout << "a big dangerous king cobra...RUN\n";
	}
	if (roomDesc & Dragon)
	{
		cout << "a big dangerous dragon...RUN\n";

	}
	if (roomDesc & Venomous_Spider)
	{
		cout << "a big dangerous venomous spider...RUN\n";

	}


	//treasure items
	if (roomDesc & Sword_of_Courage)
	{
		cout << "a big bright sword\n";
	}
	if (roomDesc & Hat_of_Knowledge)
	{
		cout << "A big hat holds knowledge of the universe\n";
	}
	if (roomDesc & Ring_of_Power)
	{
		cout << "One Ring to Rule them all\n";
	}
	if (roomDesc & Mystical_Beast)
	{
		cout << "Unknown creature but looks very beatiful\n";
	}

	//check the doors
	if (roomDesc & Door_North)
	{
		cout << "door to the North \n";
	}
	if (roomDesc & Door_West)
	{
		cout << "door to the West \n";
	}
	if (roomDesc & Door_South)
	{
		cout << "door to the South \n";
	}
	if (roomDesc & Door_East)
	{
		cout << "door to the East \n";
	}

}//end roomDesc function

 //BuildRoom
void BuildRooms(unsigned int rooms[][4])
{
	//first row
	//room 1
	rooms[0][0] = Door_East | Door_South;
	//room 2
	rooms[0][1] = Door_East | Door_South | Door_West;
	//room 3
	rooms[0][2] = Door_East | Door_South | Door_West | King_Cobra;
	//room 4
	rooms[0][3] = Door_South | Door_West | Sword_of_Courage;

	//second row
	//room 5
	rooms[1][0] = Door_East | Door_South | Door_North;
	//room 6
	rooms[1][1] = Door_East | Door_South | Door_North | Door_West;
	//room 7
	rooms[1][2] = Door_East | Door_South | Door_North | Door_West;
	//room 8
	rooms[1][3] = Door_South | Door_North | Door_West | Dragon;

	//third row
	//room 9
	rooms[2][0] = Door_East | Door_South | Door_North | Hat_of_Knowledge;
	//room 10
	rooms[2][1] = Door_East | Door_South | Door_North | Door_West;
	//room 11
	rooms[2][2] = Door_East | Door_South | Door_North | Door_West | Ring_of_Power;
	//room 12
	rooms[2][3] = Door_South | Door_North | Door_West | Venomous_Spider;

	//fourth row
	//room 13
	rooms[3][0] = Door_East | Door_North;
	//room 14
	rooms[3][1] = Door_East | Door_North | Door_West;
	//room 15
	rooms[3][2] = Door_East | Door_North | Door_West | Mystical_Beast;
	//room 16
	rooms[3][3] = Door_North | Door_West;
}



//Room 1 description (row 0, 0)
void DescribeRoom_01()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 1: This is where you start." << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}

//Room 2 description (row 0, 1)
void DescribeRoom_02()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 2: The path toward darkness." << endl;
	cout << "nothing here...." << endl;
	cout << "Keep going!!!" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 3 description (row 0, 2)
void DescribeRoom_03()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 3: a big king cobra appears" << endl;
	cout << "Fight the cobra or move on" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 4 description (row 0, 3)
void DescribeRoom_04()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 4: Grave of Swords" << endl;
	cout << "Hidden Treasure" << endl;
	cout << "Can you find it?" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 5 description (row 1, 0)
void DescribeRoom_05()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 5: Room of Illusion" << endl;
	cout << "enter the right door" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 6 description (row 1, 1)
void DescribeRoom_06()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 6: nothing around here!!!" << endl;
	cout << "moving on...." << endl;
	cout << "....-_-...." << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 7 description (row 1, 2)
void DescribeRoom_07()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 7: Lake of Hope" << endl;
	cout << "This lake is bright as the moon shines on it." << endl;
	cout << "Something moving below the surface, becareful...."<< endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 8 description (row 1, 3)
void DescribeRoom_08()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 8: Sleepy Forest" << endl;
	cout << "BEHOLD THE MIGHTY FIRE DRAGON" << endl;
	cout << "Keep Quiet and go through or fight and die...." << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 9 description (row 2, 0)
void DescribeRoom_09()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 9: The library" << endl;
	cout << "This library hold the knowledge of mankind" << endl;
	cout << "A hidden treasure (hat)... " << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 10 description (row 2, 1)
void DescribeRoom_10()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 10:Are you hungry?" << endl;
	cout << "There is a big table full of foods, wines in this room" << endl;
	cout << "You eat all the food on the table and keep going" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 11 description (row 2, 2)
void DescribeRoom_11()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 11: Sauron's warehouse" << endl;
	cout << "The great eye of Sauron is watching as you enter his warehouse." << endl;
	cout << "hidden treasure (ring)" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 12 description (row 2, 3)
void DescribeRoom_12()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 12: Spider....." << endl;
	cout << "Fight to get your reward or run like a chicken!!!" << endl;
	cout << "......................" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 13 description (row 3, 0)
void DescribeRoom_13()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "" << endl;
	cout << "An empty Room" << endl;
	cout << "keep going..." << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 14 description (row 3, 1)
void DescribeRoom_14()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 14: RNGesus" << endl;
	cout << "This room is random" << endl;
	cout << "everything in this room is so random..." << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 15 description (row 3, 2)
void DescribeRoom_15()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 15: Is that a big unicorn" << endl;
	cout << "Ride to Unicorn" << endl;
	cout << "what a big beatiful mystical beast!!!" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl << endl;
}
//Room 16 description (row 3, 3)
void DescribeRoom_16()
{
	cout << "===***===***===***===***===***===***===***===***===" << endl;
	cout << "Room 16: Exit door" << endl;
	cout << "===***===***===***===***===***===***===***===***===" << endl;
}

int TakeItem(unsigned int rooms[4][4], int row, int col, string Object)
{
	if ((Object == "SWORD") && (rooms[row][col] & Sword_of_Courage))
	{
		rooms[row][col] ^= Sword_of_Courage;
		return Sword_of_Courage;
	}
	if ((Object == "HAT") && (rooms[row][col] & Hat_of_Knowledge))
	{
		rooms[row][col] ^= Hat_of_Knowledge;
		return Hat_of_Knowledge;
	}
	if ((Object == "RING") && (rooms[row][col] & Ring_of_Power))
	{
		rooms[row][col] ^= Ring_of_Power;
		return Ring_of_Power;
	}
	if ((Object == "BEAST") && (rooms[row][col] & Mystical_Beast))
	{
		rooms[row][col] ^= Mystical_Beast;
		return Mystical_Beast;
	}
	return 0;

}
int FightFunction(unsigned int rooms[4][4], int row, int col, string creature)
{
	if ((creature == "COBRA") && (rooms[row][col] & King_Cobra))
	{

		return King_Cobra;
	}
	if ((creature == "DRAGON") && (rooms[row][col] & Dragon))
	{
		return Dragon;
	}
	if ((creature == "SPIDER") && (rooms[row][col] & Venomous_Spider))
	{
		return Venomous_Spider;
	}
	return 0;
}