/*******************************************************************
*   CS 121 Programming Assignment 3
*   File: Program_3
*   Author: Benjamin Hoang
*   Desc: Grade a multiple-choice exam with 20 questions.
*   Date: 11/11/2015
*
*   I attest that this program is entirely my own work
*****************************************************************/


#include<iostream> //this is function for cout and cin
#include<string>
#include<fstream>
using namespace std;
void Grading_Function();//Grade student answers

int main()
{

	Grading_Function();//call function grading_function

	return 0;
}

void Grading_Function()
{


	//variables
	string ID, Student_answers;
	string Test_key;
	ifstream Grades_infile;
	ofstream Grades_outfile;
	double count = 0;
	int answers_size, student_size, check_count;
	double student_grade = 0.0;
	bool end_of_line_flag = false;

	Grades_outfile.open("GradeOutput.txt");//Create output.txt file
	if (!Grades_outfile.is_open())exit(0);//check if file opens correctly
	Grades_infile.open("exams.txt");
	if (!Grades_infile.is_open())exit(0); //check if file opens correctly
	getline(Grades_infile, Test_key); //this will skip the first line and grab Test_Key
	cout << "Answers: " << Test_key << endl;
	answers_size = Test_key.size();
	while (!Grades_infile.eof())//grading answers from input file.
	{
		if(Grades_infile.eof()==true)
		{
			end_of_line_flag = true;
		}
	
		
		Grades_infile >> ID >> Student_answers;
		student_size = Student_answers.size();
		if (student_size < answers_size && end_of_line_flag== false)
		{
			Grades_outfile << "ID: " << ID << "	Too few Answers " << endl;
			//cout << "ID: " << ID << "	Too few Answers " << endl;
		}
		else if (student_size > answers_size && end_of_line_flag == false)
		{
			Grades_outfile << "ID: " << ID << "	Too Many Answers " << endl;
			//cout << "ID: " << ID << "	Too Many Answers " << endl;
		}
		else if (student_size == answers_size && end_of_line_flag == false)
		{
			string check_value = "abcdefABCDEF";
			check_count = Student_answers.find_first_not_of(check_value);

			if (check_count != -1)//check for invalid answers
			{
				Grades_outfile << "ID: " << ID << "	Invalid Answer " << endl;
				//cout << "ID: " << ID << "	Invalid Answer " << endl;
			}

			if (check_count == -1)//check for invalid answers
			{
				for (int k = 0; k < answers_size; k++)
				{
					if (Test_key[k] == Student_answers[k])
					{
						count++;
					}
					student_grade = (count / 20) * 100;
				}
				Grades_outfile << "ID: " << ID << "	correct answers: " << count << "	Grade = " << student_grade << endl;
				//cout << "ID: " << ID << "	correct answers: " << count <<"	Grade = "<<student_grade<< endl;
				count = 0;//reset count
			}
		}
	
	}
	
	Grades_infile.close();//close input file
	Grades_outfile.close();//close output file
	
}