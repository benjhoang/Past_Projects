#include<iostream>  //defines cin and cout functions
#include<cmath> // contains mathematical functions
#include<string>		//header files
using namespace std;  //identifies the namespace for standard
int main()	//the main function for the c++ program
{//open brace for the main function
	
	//declare variables after this line
	string fName, lName, empId; 
	double payrate, grossPay, netPay, Deductions, Hours;
	const double stateTax= 0.05;
	const double fedTax = 0.08;
	const double ins=100;
	//convert algorithm to C++

	//promt user for informations
	cout<<"pls enter employee First name"<<endl;
	cin>>fName;
	cout<<"pls enter employee Last name"<<endl;
	cin>>lName;
	cout<<"pls enter employee ID"<<endl;
	cin>>empId;
	cout<<"pls enter employee work hours"<<endl;
	cin>>Hours;
	cout<<"pls enter employee payrate"<<endl;
	cin>>payrate;

	//calculate netpay
	grossPay=payrate*Hours;
	Deductions= ins+(grossPay*stateTax)+(grossPay*fedTax);
	netPay=grossPay-Deductions;
	
	//display output
	cout<<"employee name: "<<fName<<" , "<<lName<<endl;
	cout<<"employee ID: "<<empId<<endl;
	cout<<"employee hours: "<<Hours<<endl;
	cout<<"employee payrate: "<<payrate<<endl;
	cout<<"employee netpay: $"<<netPay<<endl;

	return 0; //indicate complete of program

}//end brace for the main function
