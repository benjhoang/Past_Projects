//Benjamin Hoang
//TICTAC GAME
//CLASS: CIS 201
//DATE: 12/10/2013
#include<iostream> // contains thre function definition for cout and cin
#include<cmath> // contains mathematical functions
#include<string> // contains the string functions
using namespace std; 
int main() 
{
	//all global variables
	int x, y;
	int counter1=0;
	int counter2=0;
	bool isNumber= true;


	//declare a 2 dimemsions array
	char tictacGrid[3][3]={'*','*','*','*','*','*','*','*','*'};

	for (int i=0; i< 9; i++)
		{	//check if player 1 or player 2 turns
			if(counter1 == counter2)
			{
			cout<<"player 1: pls enter 1st location"<<endl;
			cin>>x;
				while (x<0 || x>2)
					{
						cout<<"re-enter 1st location"<<endl;
						cin>>x;
					}
								
			cout<<"player 1: pls enter 2nd location"<<endl;
				cin>>y;
				while (y<0 || y>2)
					{
						cout<<"re-enter 2nd location"<<endl;
						cin>>y;
					}
			//ini. x to player 1
				//check if any player already taken this coordinate
					
					if(tictacGrid[x][y]=='x'||tictacGrid[x][y]=='o')
								{		cout<<"already taken!!!!"<<endl;
										cout<<"pls enter 1st location again!!!"<<endl;
										cin>>x;
										while (x<0 || x>2)
										{
										cout<<"re-enter 1st location"<<endl;
										cin>>x;
										}
										cout<<"pls enter 2nd location again!!!"<<endl;
										cin>>y;
										while (y<0 || y>2)
										{
										cout<<"re-enter 2nd location"<<endl;
										cin>>y;
										}	
								}
				tictacGrid[x][y]='x';
				counter1=counter1 + 1;
								
									//graphic grid
									cout<<tictacGrid[0][0]<<" | "<<tictacGrid[0][1]<<" | "<<tictacGrid[0][2]<<endl;
									cout<<"----------"<<endl;
									cout<<tictacGrid[1][0]<<" | "<<tictacGrid[1][1]<<" | "<<tictacGrid[1][2]<<endl;
									cout<<"----------"<<endl;
									cout<<tictacGrid[2][0]<<" | "<<tictacGrid[2][1]<<" | "<<tictacGrid[2][2]<<endl;
									
										//check if player 1 win 
										while ( tictacGrid[0][0]=='x' && tictacGrid[0][1]=='x' && tictacGrid[0][2]=='x'||
											    tictacGrid[1][0]=='x' && tictacGrid[1][1]=='x' && tictacGrid[1][2]=='x'||
												tictacGrid[2][0]=='x' && tictacGrid[2][1]=='x' && tictacGrid[2][2]=='x'||
												tictacGrid[0][0]=='x' && tictacGrid[1][0]=='x' && tictacGrid[2][0]=='x'||
												tictacGrid[0][1]=='x' && tictacGrid[1][1]=='x' && tictacGrid[2][1]=='x'||
												tictacGrid[0][2]=='x' && tictacGrid[1][2]=='x' && tictacGrid[2][2]=='x'||
												tictacGrid[0][2]=='x' && tictacGrid[1][1]=='x' && tictacGrid[0][2]=='x'||
												tictacGrid[0][0]=='x' && tictacGrid[1][1]=='x' && tictacGrid[2][2]=='x' )
												{
													cout<<"player 1 wins"<<endl;
													
													return 0;
													
												}
										//check if player 2 win
										while  ( tictacGrid[0][0]=='o' && tictacGrid[0][1]=='o' && tictacGrid[0][2]=='o'||
											    tictacGrid[1][0]=='o' && tictacGrid[1][1]=='o' && tictacGrid[1][2]=='o'||
												tictacGrid[2][0]=='o' && tictacGrid[2][1]=='o' && tictacGrid[2][2]=='o'||
												tictacGrid[0][0]=='o' && tictacGrid[1][0]=='o' && tictacGrid[2][0]=='o'||
												tictacGrid[0][1]=='o' && tictacGrid[1][1]=='o' && tictacGrid[2][1]=='o'||
												tictacGrid[0][2]=='o' && tictacGrid[1][2]=='o' && tictacGrid[2][2]=='o'||
												tictacGrid[0][2]=='o' && tictacGrid[1][1]=='o' && tictacGrid[0][2]=='o'||
												tictacGrid[0][0]=='o' && tictacGrid[1][1]=='o' && tictacGrid[2][2]=='o' )
												{
													cout<<"player 2 wins"<<endl;
													return 0;
													

												}
			}

	
		




	//check if player 1 or player 2 turn
			if(counter2  < counter1)
			{
			cout<<"player 2: pls enter 1st coordinate"<<endl;
			cin>>x;
				while (x<0 || x>2)
					{
						cout<<"re-enter x"<<endl;
						cin>>x;
					}

			cout<<"player 2: pls enter 2nd coordinate"<<endl;
				cin>>y;
				while (y<0 || y>2)
					{
						cout<<"re-enter y"<<endl;
						cin>>y;
					}
			//init. x to player 2
				//check if any player already taken this coordinate
					if(tictacGrid[x][y]=='x'||tictacGrid[x][y]=='o')
								{		cout<<"already taken!!!!"<<endl;
										cout<<"pls enter 1st location again!!!"<<endl;
										cin>>x;
										while (x<0 || x>2)
										{
										cout<<"re-enter 1st location"<<endl;
										cin>>x;
										}
										cout<<"pls enter 2nd location again!!!"<<endl;
										cin>>y;
										while (y<0 || y>2)
										{
										cout<<"re-enter 2nd location"<<endl;
										cin>>y;
										}	
								}
			tictacGrid[x][y]='o';
			counter2=counter2 + 1;
									//graphic grid
									cout<<tictacGrid[0][0]<<" | "<<tictacGrid[0][1]<<" | "<<tictacGrid[0][2]<<endl;
									cout<<"----------"<<endl;
									cout<<tictacGrid[1][0]<<" | "<<tictacGrid[1][1]<<" | "<<tictacGrid[1][2]<<endl;
									cout<<"----------"<<endl;
									cout<<tictacGrid[2][0]<<" | "<<tictacGrid[2][1]<<" | "<<tictacGrid[2][2]<<endl;
									
										//check if player 1 wins 
										while ( tictacGrid[0][0]=='x' && tictacGrid[0][1]=='x' && tictacGrid[0][2]=='x'||
											    tictacGrid[1][0]=='x' && tictacGrid[1][1]=='x' && tictacGrid[1][2]=='x'||
												tictacGrid[2][0]=='x' && tictacGrid[2][1]=='x' && tictacGrid[2][2]=='x'||
												tictacGrid[0][0]=='x' && tictacGrid[1][0]=='x' && tictacGrid[2][0]=='x'||
												tictacGrid[0][1]=='x' && tictacGrid[1][1]=='x' && tictacGrid[2][1]=='x'||
												tictacGrid[0][2]=='x' && tictacGrid[1][2]=='x' && tictacGrid[2][2]=='x'||
												tictacGrid[0][2]=='x' && tictacGrid[1][1]=='x' && tictacGrid[0][2]=='x'||
												tictacGrid[0][0]=='x' && tictacGrid[1][1]=='x' && tictacGrid[2][2]=='x' )
												{
													cout<<"player 1 win"<<endl;
													return 0;

												}
										//check if player 2 wins
										while ( tictacGrid[0][0]=='o' && tictacGrid[0][1]=='o' && tictacGrid[0][2]=='o'||
											    tictacGrid[1][0]=='o' && tictacGrid[1][1]=='o' && tictacGrid[1][2]=='o'||
												tictacGrid[2][0]=='o' && tictacGrid[2][1]=='o' && tictacGrid[2][2]=='o'||
												tictacGrid[0][0]=='o' && tictacGrid[1][0]=='o' && tictacGrid[2][0]=='o'||
												tictacGrid[0][1]=='o' && tictacGrid[1][1]=='o' && tictacGrid[2][1]=='o'||
												tictacGrid[0][2]=='o' && tictacGrid[1][2]=='o' && tictacGrid[2][2]=='o'||
												tictacGrid[0][2]=='o' && tictacGrid[1][1]=='o' && tictacGrid[0][2]=='o'||
												tictacGrid[0][0]=='o' && tictacGrid[1][1]=='o' && tictacGrid[2][2]=='o' )
												{
													cout<<"player 2 win"<<endl;
													return 0;

												}
			}
	}

	cout<<"tie!!!"<<endl;
	return 0;
		}